package com.br.tdd;

import java.sql.ResultSet;

public class Calculadora {

    public static int soma(int numero, int numero2){
        int resultado = numero + numero2;
        return resultado;
    }
    public static int divisao(int numero, int numero2) {
        int resultado = numero / numero2;
        return resultado;
    }


    public static float divisaoFloat(int numero, int numero2) {
        float resultado = numero / numero2;
        return resultado;
    }

    public static int multiplicacao(int numero, int numero2) {
        int resultado = numero * numero2;
        return resultado;
    }
    public static float multiplicacao(float numero, float numero2) {
        float resultado = numero * numero2;
        return resultado;
    }
}
