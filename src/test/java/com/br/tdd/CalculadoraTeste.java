package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumeros(){
        int resultado = Calculadora.soma(2, 2);

        Assertions.assertEquals(4,resultado);
    }
    @Test
    public void testarDivisaoDeDoisNumeros() {
        int resultado = Calculadora.divisao(10, 2);

        Assertions.assertEquals(5, resultado);
    }
    @Test
    public void testarDivisaoDeDoisNumerosFloat() {
        float resultado =  Calculadora.divisaoFloat(10,5);

        Assertions.assertEquals(2, resultado);
    }
    @Test
    public void testarMultiplicacaoDeDoisNumerosFloat() {
        float resultado = Calculadora.multiplicacao(10, 2);

        Assertions.assertEquals(20, resultado);
    }
    @Test
    public void testarMultiplicacaoDeDoisNumeros() {
        int resultado = Calculadora.multiplicacao(10, 2);

        Assertions.assertEquals(20, resultado);
    }

}
